export function showWinnerModal(fighter) {
  
  const name = `${fighter.name} Winner!!!!!`;
  const blockWinner = document.createElement('div');
    blockWinner.classList.add('modal-winner__user');
    blockWinner.innerText = `Winner!!!!! Name: ${fighter.name}`;

  const result = {
    title: name,
    bodyElement: blockWinner,
    onClose: () => {
      window.location.reload()
    }
  };
  showModal(result);
}
